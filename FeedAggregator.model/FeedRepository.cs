﻿using System;
using System.Collections.Generic;
using FeedAggregator.model.Poco;

namespace FeedAggregator.model {
  public interface IFeedRepository<T> {
    T getFeed();
  }
  public class MemoryFeedRepository : IFeedRepository<OfferFeed> {
    public OfferFeed getFeed() {
      return new OfferFeed() {
        RowData = new List<Offer>() {
          new Offer() {
            id = 37,
            name = @"Fishdom Dive Deep",
            tracking_url = @"http:\/\/aptrk.com\/c\/82\/s0os168o-r262-66r1-3r71-5pp92n99qonn\/9s36q6r5.html",
            android_package_name = @"com.playrix.fishdomdd.gplay",
            ios_bundle_id = 664575829,
            icon_url= @"https:\/\/lh3.googleusercontent.com\/0MLteWwAYrO63ciEBAHxVh3kbM5lKb6Qv-k5Dhdh5mY7iHJjR-uHx55_iWpkk4K4CQ=w300",
            description= @"A unique blend of city-building and farming experiences!",
            categories = new []{ "Entertainment", "Games - Adventure", "Games - Casual", "Games - Simulation", "Games"},
            campaigns = new List<Poco.Campaign>() {
                new Poco.Campaign() {
                  id = "338367034b150a1b96886bfbbcec204d",
                  incent = 0,
                  payout = 0.26,
                  platform = "Android",
                  countries = new [] { "AU"},
                },
                new Poco.Campaign() {
                  id = "79dce45e3ed60c5da776b39d548cd008",
                  incent = 0,
                  payout = 0.29,
                  platform = "Android",
                  countries = new [] { "CA"},
                },
                new Poco.Campaign() {
                  id = "66bf9437f4b649a9a79a72c5cfa46274",
                  incent = 0,
                  payout = 0.19,
                  platform = "Android",
                  countries = new [] { "DE"},
                },
                new Poco.Campaign() {
                  id = "2798f491de625293b191a0cf1a481e26",
                  incent = 0,
                  payout = 0.15,
                  platform = "Android",
                  countries = new [] { "NZ"},
                },
            },
          },
          new Offer() {
          id = 50,
            name = @"Chain Chronicle",
            tracking_url = @"http:\/\/aptrk.com\/c\/05\/s0os168o-r262-66r1-3r71-5pp92n99qonn\/r518o3s6.html",
            android_package_name = @"com.sega.chainchronicle",
            ios_bundle_id = 935189878,
            icon_url= @"https:\/\/lh4.ggpht.com\/aEqnYuZlrahwO0WC-Q79qnKxnQlfqnXy3g0SKeTl7eG11E0gtHDyDx702o_efs92lQ=w300",
            description= @"Over 4 Million Downloads In Japan! Forge Your Story And Train Your Perfect Army! Control a lone commander who must protect the world of Yggdra from the hordes of the invading Black Army in this first-",
            categories = new []{ "Entertainment", "Games - Role Playing", "Games - Strategy", "Games"},
            campaigns = new List<Poco.Campaign>() {
                new Poco.Campaign() {
                  id = "db3fe31dff6865a8b015bfb9a491ad4b",
                  incent = 0,
                  payout = 0.98,
                  platform = "Android",
                  countries = new [] { "JP"},
                },
            }
          },
          new Offer() {
            id = 56,
            name = @"The Local Phone Book",
            tracking_url = @"http:\/\/aptrk.com\/c\/01\/s0os168o-r262-66r1-3r71-5pp92n99qonn\/09n6p186.html",
            android_package_name = @"de.dasoertliche.android",
            ios_bundle_id = 330158440,
            icon_url= @"https:\/\/lh3.ggpht.com\/hGI_kwNWMkuLvZhnxK_WlK5Si0Sx9UkB8agijzudeBESUv4kZMSVQDb2GMrXzr0Amg=w300",
            description= "The famous telephone book and yellow page application \"Das Oertliche\" for Germany - optimized for iPhone, iPad and iPod.",
            categories = new []{ "Books & Reference", "Navigation"},
            campaigns = new List<Poco.Campaign>() {
                new Poco.Campaign() {
                  id = "815f195606242564bc88ea24b17c58a1",
                  incent = 0,
                  payout = 0.35,
                  platform = "Android",
                  countries = new [] { "DE"},
                },
            }
          },
          new Offer() {
            id = 328,
            name = @"Slots - House of Fun",
            tracking_url = @"http:\/\/aptrk.com\/c\/873\/s0os168o-r262-66r1-3r71-5pp92n99qonn\/o0q11ns6.html",
            android_package_name = @"com.pacificinteractive.HouseOfFun",
            ios_bundle_id = 586634331,
            icon_url= @"http:\/\/is5.mzstatic.com\/image\/pf\/us\/r30\/Purple7\/v4\/db\/fc\/04\/dbfc0459-5bb8-6a57-0df0-05a0768f43b9\/mzl.kuprvwvh.200x200-75.png",
            description= "Premier casino app that delivers realistic slot games online",
            categories = new []{ "Entertainment", "Games - Card", "Games - Casino", "Games"},
            campaigns = new List<Poco.Campaign>() {
                new Poco.Campaign() {
                  id = "cea45720a5853256ec5cbc7e4fcfb2bb",
                  incent = 0,
                  payout = 0.37,
                  platform = "Android",
                  countries = new [] { "AU"},
                },
                new Poco.Campaign() {
                  id = "6ca1abbf7fc1725cb05c0fa79eba6f2d",
                  incent = 0,
                  payout = 0.39,
                  platform = "Android",
                  countries = new [] { "AU"},
                },
            }
          },
          new Offer() {
            id = 348,
            name = @"iFood - Delivery de Comida",
            tracking_url = @"http:\/\/aptrk.com\/c\/893\/s0os168o-r262-66r1-3r71-5pp92n99qonn\/2qqpon71-96nn-9019-np6r-48ss69o05noo\/159ono0n.html",
            android_package_name = @"br.com.brainweb.ifoo",
            ios_bundle_id = 483017239,
            icon_url= @"http:\/\/a1121.phobos.apple.com\/us\/r30\/Purple4\/v4\/b3\/25\/0e\/b3250edc-caf4-1cdc-eb13-eff5fddbb600\/mzl.iqifhcmu.200x200-75.png",
            description= "Procurar card\u00e1pios para pedir comida virou coisa do passado. Com o iFood, voc\u00ea faz o pedido rapidamente em qualquer restaurante que atenda a sua regi\u00e3o.",
            categories = new []{ "Lifestyle"},
            campaigns = new List<Poco.Campaign>() {
                new Poco.Campaign() {
                  id = "bc072199d90d9309b8b7a9c2004c56dd",
                  incent = 0,
                  payout = 2.20,
                  platform = "iPhone",
                  countries = new [] { "AR"},
                },
                new Poco.Campaign() {
                  id = "af94618be51c10951b203ffeb5afb511",
                  incent = 0,
                  payout = 0.17,
                  platform = "iPhone",
                  countries = new [] { "BR"},
                },
            }
          },
        }
      };
    }
  }
}
