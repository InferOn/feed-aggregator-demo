﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedAggregator.model
{
	public class Feed
	{
		public List<Car> RowData { get; internal set; }
	}

	public class CampaignFeed
	{
		public List<Campaign> RowData { get; internal set; }
	}

	public enum eCampaignType
	{
		None = 0,
		BannerAD = 1,
		EmailAD = 2,
		FloorAD = 3,
		Interstizial = 4
	}

	public class Campaign
	{
		public int id { get; set; }
		public string name { get; set; }
		public eCampaignType type { get; set; }
		public int Impressions { get; set; }
		public int Clicks { get; set; }
		public double CTR { get; set; }
	}

	public class Car
	{
		public string maker { get; set; }
		public string model { get; set; }
		public int price { get; set; }
	}
}

