﻿using System.Collections.Generic;

namespace FeedAggregator.model.Poco {
  public class Campaign { 
    public string id { get; set; }
    public int incent { get; set; }
    public double payout { get; set; }
    public string platform { get; set; }
    public IEnumerable<string> countries { get; set; }
  }
  public class Offer {
    public int id{ get; set; }
    public string name { get; set; }
    public string tracking_url { get; set; }
    public string android_package_name { get; set; }
    public int ios_bundle_id { get; set; }
    public string icon_url { get; set; }
    public string description { get; set; }
    public IEnumerable<string> categories { get; set; }
    public IEnumerable<Campaign> campaigns { get; set; }
  }
  public class OfferFeed {
    public IEnumerable<Offer> RowData { get; internal set; }
  }
}
