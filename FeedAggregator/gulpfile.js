﻿var gulp = require('gulp');
var less = require('gulp-less');
var watch = require('gulp-watch');
var lint = require('tslint');
var tslint = require('gulp-tslint');

gulp.task('deploy_dependency', function () {
    gulp.src([
        //"./node_modules/angular/angular.min.js",
        "./node_modules/ag-grid/dist/ag-grid.min.js",
        "./node_modules/angular-material/angular-material.min.js",
        "./node_modules/angular-animate/angular-animate.min.js",
        "./node_modules/angular-aria/angular-aria.min.js",
        //"./node_modules/angular-google-chart/ng-google-chart.min.js"
    ])
    .pipe(gulp.dest("./Scripts/")); 

    gulp.src([
        "./node_modules/ag-grid/dist/styles/*.css",
        "./node_modules/angular-material/angular-material.min.css",

    ])
    .pipe(gulp.dest("./Content/"));

    /**
     * typescript type definitions
     */
    gulp.src([
        "./node_modules/definitely-typed-angular/angular.d.ts",
        "./node_modules/definitely-typed-jquery/jquery.d.ts",
    ])
    .pipe(gulp.dest("./Scripts/typings/"));

    gulp.src([
        "./app/app.js",
    ])
    .pipe(gulp.dest("./Scripts/"));

});

gulp.task('ts-sync', function () {
    gulp.src([
        "./app/app.js",
    ])
    .pipe(gulp.dest("./Scripts/"));
});

gulp.task("ts-lint", function () {
    gulp.src("source.ts")
        .pipe(tslint({
            formatter: "verbose"
        }))
        .pipe(tslint.report())
    }
);

gulp.task('ts-watcher', function () {
    watch("./app/app.js", function () {
        gulp.run(['ts-sync', 'ts-lint']);
    });
});
