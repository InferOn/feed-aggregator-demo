﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FeedAggregator.Helpers {
  public static class Mappings {
    public static viewModels.OfferViewModel ToViewModel(this model.Poco.Offer source) {
      return new viewModels.OfferViewModel() {
        id = source.id,
        name = source.name,
        android_package_name =source.android_package_name,
        campaigns = source.campaigns.ToViewModel(),
        categories = source.categories,
        description = source.description,
        icon_url = source.icon_url,
        ios_bundle_id = source.ios_bundle_id,
        tracking_url = source.tracking_url
      };
    }
    public static IEnumerable<viewModels.CampaignViewModel> ToViewModel(this IEnumerable<model.Poco.Campaign> source) {
      return source.Select(x => new viewModels.CampaignViewModel() { 
        countries = x.countries,
        id = x.id,
        incent = x.incent,
        payout = x.payout,
        platform = x.platform
      }).ToArray();
    }
  }
}