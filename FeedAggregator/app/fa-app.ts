﻿/// <reference path="../Scripts/typings/angular.d.ts" />
/// <reference path="../Scripts/typings/TypeLite.Net4.d.ts"/>

var agGrid: any;
agGrid.initialiseAgGridWithAngular1(angular);
"use strict";
var feedapp = angular.module("feedapp", ["agGrid", "ngMaterial"])
  .controller("ToolbarTopController", FeedAggregator.ToolbarTopController)
  .controller("ContentController", FeedAggregator.ContentController)
  .controller("FeedController", FeedAggregator.FeedController)
  .config(function ($mdThemingProvider) {
    $mdThemingProvider.theme('default').primaryPalette('blue');
  })

