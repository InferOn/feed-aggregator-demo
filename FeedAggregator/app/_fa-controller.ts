﻿namespace FeedAggregator {
  "use strict";

  export class FeedController {
    static $inject = ["$scope", "rowData", "columnDefs"];
    gridOptions: any;

    constructor(private $scope: any, private rowData: FeedAggregator.viewModels.offerViewModel[], private columnDefs: any) {
      this.rowData = rowData;
      this.columnDefs = columnDefs;
      this.initGrid(this.rowData, this.columnDefs);
    }
    private initGrid(rows: FeedAggregator.viewModels.offerViewModel[], columnDefs: any): void {
      this.$scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: rows,
        rowSelection: 'multiple',
        enableColResize: true,
        enableSorting: true,
        enableFilter: true,
        rowHeight: 30,
        onModelUpdated: () => {
          var model = this.$scope.gridOptions.api.getModel();
          var totalRows = this.$scope.gridOptions.rowData.length;
          var processedRows = model.getRowCount();
          this.$scope.rowCount = processedRows.toLocaleString() + ' / ' + totalRows.toLocaleString();
        },
        suppressRowClickSelection: true
      };
    }
  };

  export class ToolbarTopController {
    static $inject = ["$scope", "$timeout", "$mdSidenav", "$log"];
    name: string;
    constructor(private $scope: any, private $timeout: any, private $mdSidenav: any, private $log: any) {
      this.name = "demo";
      this.$scope.toggleLeft = this.buildToggler('left');
    }
    buildToggler(componentId) {
      return () => {
        this.$mdSidenav(componentId).toggle();
      };
    }
  };

  export class ContentController {
    static $inject = ["$scope", "$timeout", "$mdSidenav", "$log"];
    sidebarTitle: string = "Sidebar";
    constructor(private $scope: any, private $timeout: any, private $mdSidenav: any, private $log: any) {
      this.$scope.toggleLeft = this.buildToggler('left');
    }

    buildToggler(componentId) {
      return () => {
        this.$mdSidenav(componentId).toggle();
      };
    }
  }
}