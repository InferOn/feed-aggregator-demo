﻿using System.Collections.Generic;
using TypeLite;

namespace FeedAggregator.viewModels {
  [TsClass()]
  public class OfferFeedViewModel {
    public IEnumerable<OfferViewModel> RowData { get; internal set; }
  }
}