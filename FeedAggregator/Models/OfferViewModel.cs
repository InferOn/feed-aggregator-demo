﻿using System.Collections.Generic;
using TypeLite;

namespace FeedAggregator.viewModels {

  [TsClass()]
  public class OfferViewModel {
    public int id { get; set; }
    public string name { get; set; }
    public string tracking_url { get; set; }
    public string android_package_name { get; set; }
    public int ios_bundle_id { get; set; }
    public string icon_url { get; set; }
    public string description { get; set; }
    public IEnumerable<string> categories { get; set; }
    public IEnumerable<CampaignViewModel> campaigns { get; set; }
  }
}