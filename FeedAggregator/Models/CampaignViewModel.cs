﻿using System.Collections.Generic;
using TypeLite;

namespace FeedAggregator.viewModels {

  [TsClass()]
  public class CampaignViewModel {
    public string id { get; set; }
    public int incent { get; set; }
    public double payout { get; set; }
    public string platform { get; set; }
    public IEnumerable<string> countries { get; set; }
  }
}