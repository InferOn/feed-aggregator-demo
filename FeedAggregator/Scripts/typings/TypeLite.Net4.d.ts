﻿
 
 



/// <reference path="Enums.ts" />

declare module FeedAggregator.viewModels {
	interface campaignViewModel {
		countries: string[];
		id: string;
		incent: number;
		payout: number;
		platform: string;
	}
	interface offerFeedViewModel {
		RowData: FeedAggregator.viewModels.offerViewModel[];
	}
	interface offerViewModel {
		android_package_name: string;
		campaigns: FeedAggregator.viewModels.campaignViewModel[];
		categories: string[];
		description: string;
		icon_url: string;
		id: number;
		ios_bundle_id: number;
		name: string;
		tracking_url: string;
	}
}


