var FeedAggregator;
(function (FeedAggregator) {
    "use strict";
    var FeedController = (function () {
        function FeedController($scope, rowData, columnDefs) {
            this.$scope = $scope;
            this.rowData = rowData;
            this.columnDefs = columnDefs;
            this.rowData = rowData;
            this.columnDefs = columnDefs;
            this.initGrid(this.rowData, this.columnDefs);
        }
        FeedController.prototype.initGrid = function (rows, columnDefs) {
            var _this = this;
            this.$scope.gridOptions = {
                columnDefs: columnDefs,
                rowData: rows,
                rowSelection: 'multiple',
                enableColResize: true,
                enableSorting: true,
                enableFilter: true,
                rowHeight: 30,
                onModelUpdated: function () {
                    var model = _this.$scope.gridOptions.api.getModel();
                    var totalRows = _this.$scope.gridOptions.rowData.length;
                    var processedRows = model.getRowCount();
                    _this.$scope.rowCount = processedRows.toLocaleString() + ' / ' + totalRows.toLocaleString();
                },
                suppressRowClickSelection: true
            };
        };
        FeedController.$inject = ["$scope", "rowData", "columnDefs"];
        return FeedController;
    }());
    FeedAggregator.FeedController = FeedController;
    ;
    var ToolbarTopController = (function () {
        function ToolbarTopController($scope, $timeout, $mdSidenav, $log) {
            this.$scope = $scope;
            this.$timeout = $timeout;
            this.$mdSidenav = $mdSidenav;
            this.$log = $log;
            this.name = "demo";
            this.$scope.toggleLeft = this.buildToggler('left');
        }
        ToolbarTopController.prototype.buildToggler = function (componentId) {
            var _this = this;
            return function () {
                _this.$mdSidenav(componentId).toggle();
            };
        };
        ToolbarTopController.$inject = ["$scope", "$timeout", "$mdSidenav", "$log"];
        return ToolbarTopController;
    }());
    FeedAggregator.ToolbarTopController = ToolbarTopController;
    ;
    var ContentController = (function () {
        function ContentController($scope, $timeout, $mdSidenav, $log) {
            this.$scope = $scope;
            this.$timeout = $timeout;
            this.$mdSidenav = $mdSidenav;
            this.$log = $log;
            this.sidebarTitle = "Sidebar";
            this.$scope.toggleLeft = this.buildToggler('left');
        }
        ContentController.prototype.buildToggler = function (componentId) {
            var _this = this;
            return function () {
                _this.$mdSidenav(componentId).toggle();
            };
        };
        ContentController.$inject = ["$scope", "$timeout", "$mdSidenav", "$log"];
        return ContentController;
    }());
    FeedAggregator.ContentController = ContentController;
})(FeedAggregator || (FeedAggregator = {}));
/// <reference path="../Scripts/typings/angular.d.ts" />
/// <reference path="../Scripts/typings/TypeLite.Net4.d.ts"/>
var agGrid;
agGrid.initialiseAgGridWithAngular1(angular);
"use strict";
var feedapp = angular.module("feedapp", ["agGrid", "ngMaterial"])
    .controller("ToolbarTopController", FeedAggregator.ToolbarTopController)
    .controller("ContentController", FeedAggregator.ContentController)
    .controller("FeedController", FeedAggregator.FeedController)
    .config(function ($mdThemingProvider) {
    $mdThemingProvider.theme('default').primaryPalette('blue');
});
//# sourceMappingURL=app-bundle.js.map