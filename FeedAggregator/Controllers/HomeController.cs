﻿using System.Linq;
using System.Web.Mvc;
using FeedAggregator.model;
using FeedAggregator.viewModels;
using FeedAggregator.Helpers;
using FeedAggregator.model.Poco;

namespace FeedAggregator.Controllers {
  public class HomeController : Controller {
    private IFeedRepository<OfferFeed> _offerRepository;

    public HomeController(IFeedRepository<OfferFeed> offerRepository) {
      _offerRepository = offerRepository;
    }
    public ActionResult Index() {
      return View(new OfferFeedViewModel() {
        RowData = _offerRepository
                .getFeed()
                  .RowData
                    .Select(x => x.ToViewModel()).ToList()
      });
    }
  }
}